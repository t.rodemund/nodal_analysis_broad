import time
import random
import numpy as np
import shapely
from shapely.geometry import box as sbox

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from line_class import Line

import toolbox as tb

# from constants import CONDUCTANCE_LINE
# from constants import CONDUCTANCE_TUNNELING
from constants import CURRENT
from constants import LAYER_HEIGHT

class Network:

    def __init__(self, G_line=.0, G_tunnel=.0):
        self.width = None
        self.length = None
        self.lines = []
        self.n_lines = None
        self.clusters = []

        self.conductance_line = G_line # G0
        self.conductance_tunneling = G_tunnel # G0


    ###########################################
    # Generation
    ###########################################

    def create_lines(self, length, width, n_lines, linelength, linewidth):

        self.length = length
        self.width = width
        self.n_lines = n_lines

        for i in range(n_lines):
            # print('\ni = ', i)
            x0 = random.random() * length
            y0 = random.random() * width

            r0 = [x0, y0]
            dr = tb.randomDirection() * linelength

            l = Line(r0, dr, linewidth)
            l.name = '{0}'.format(i)

            # get y-coordinate
            
            while True:
                # print(len(self.lines))
                for line in self.lines:
                    # print(line.ame)
                    is_intersecting, intersection = l.intersection_poly(line, use_y='layer')
                    # print('is_intersecting = ', is_intersecting)

                    if is_intersecting:
                        l.y = l.y + LAYER_HEIGHT
                        break
                # if for-loop gets through withoug break, we break out of while-loop
                else:
                    break
            
            
            # print(l.y)

            self.lines.append(l)

            # print('l.y = ', l.y)


    ###########################################
    # Manipulate
    ###########################################

    def cut_to_size(self):
        '''
        Cuts off all lines extending further than the x-y-dimensions of the network
        '''

        # box containing the network

        box = sbox(.0, .0, self.length, self.width)

        # checking if any lines overshoot the borders

        for line in self.lines:
            polygon = line.polygon

            # check if touches left or right
            x_coords = np.transpose(polygon.exterior.coords[:])[0 ]
            
            min_x = np.min(x_coords)
            if min_x < .0:
                line.connects_left = True
            
            max_x = np.max(x_coords)
            if max_x > self.length:
                line.connects_right = True

            intersection = box.intersection(polygon)
            line.polygon = intersection


    ###########################################
    # Analyze
    ###########################################

    def find_intersections(self, use_y='tunneling'):
        '''
        Finds all intersections and returns coordinates of the
        center of the coordinates
        '''

        # print('\n\nFIND INTERSECTIONS')

        intersection_list = []

        for i in range(self.n_lines-1):
            line_1 = self.lines[i]
            for j in range(i+1, self.n_lines):
                line_2 = self.lines[j]
                
                intersection_exists, intersection = line_1.intersection_poly(line_2, use_y=use_y)

                if intersection_exists:
                    # print()
                    # leave out last point, as it gets counted twice otherwise
                    coords = intersection.exterior.coords[:-1]
                    # print(coords)
                    # averaging the corner coords to place coord in the middle
                    center = np.mean(coords, axis=0)
                    # print('center = ', center)
                    intersection_list.append(center)
        
        return intersection_list


    def find_clusters(self):
        '''
        constructs the connectivity-matrix out of the intersections,

        this function is still incomplete
        '''

        # finding neighbors

        for i in range(self.n_lines-1):
            line_1 = self.lines[i]
            for j in range(i+1, self.n_lines):
                line_2 = self.lines[j]

                intersection_exists, intersection = line_1.intersection_poly(line_2, use_y='tunneling')

                if intersection_exists:
                    # saving friends as dicts so we don't nee to calculate the
                    # coordinates again

                    friend_1 = {
                        'name' : line_2,
                        'intersection' : intersection
                        }
                    line_1.friends.append(friend_1)

                    friend_2 = {
                        'name' : line_1,
                        'intersection' : intersection
                        }
                    line_2.friends.append(friend_2)
        

        # get clusters

        clusters = []
        lines_used = []

        while True:
            lines_left = [l for l in self.lines if l not in lines_used]

            # if no lines are left to look for clusters, the search has ended
            if len(lines_left) == 0:
                break
            
            # connecting networks
            start_line = lines_left[0]

            lines_used.append(start_line)

            cluster_now = [start_line]
            
            old_lines = [start_line]
            new_lines = []

            while True:
                for line in old_lines:
                    for friend in line.friends:
                        linefriend = friend['name']
                        if linefriend not in cluster_now and linefriend not in new_lines and linefriend not in old_lines:
                            cluster_now.append(linefriend)
                            new_lines.append(linefriend)
                            lines_used.append(linefriend)

                if len(new_lines) == 0:
                    break

                old_lines = new_lines
                new_lines = []
            
            clusters.append(cluster_now)
        
        self.clusters = clusters

    
    def only_percolating(self):
        '''
        checks if clusters percolate and deletes all but the largest
        percolating cluster
        '''
        
        clusters_percolating = []

        for c in self.clusters:
            touch_left = False
            touch_right = False
            
            # look for left
            for l in c:
                l.connects_left
                if l.connects_left:
                    touch_left = True
                    break
            
            # look for right
            for l in c:
                if l.connects_right:
                    touch_right = True
                    break
            
            if touch_left and touch_right:
                clusters_percolating.append(c)
        
        if len(clusters_percolating) > 0:
            clusters_percolating = sorted(clusters_percolating, key=len)

            largest_cluster = clusters_percolating[-1]

            self.lines = largest_cluster
            self.clusters = [largest_cluster]
            self.n_lines = len(largest_cluster)

            return True
        else:
            return False


    def calculate_conductance(self, verbose=False):
        '''
        calculates the conductance of the newtork, assuming all lines are part of a 
        percolating network
        '''

        # calculating the number of vertices

        vertice_names = []
        vertice_intersections = []

        for line_1 in self.lines:
            # sorting by x-coordinates so only neighboring vertices on a line will be connected
            line_1.friends = sorted(
                line_1.friends, 
                key=lambda x: np.mean(x['intersection'].exterior.coords, axis=0)[0]
                )

            for friend in line_1.friends:
                line_2 = friend['name']
                p = friend['intersection']

                vertice_name_now = [line_1, line_2]

                vertice_names.append(vertice_name_now)
                vertice_intersections.append(p)

        # total number of vertices
        n_vertices = len(vertice_names)

        # constructing conductance matrix

        M = np.zeros((n_vertices, n_vertices))

        # connecting vertices
        for i in range(n_vertices-1):
            vertice_1 = vertice_names[i]
            # print('\ni = {0}, v_1 = {1}'.format(i, vertice_1))

            for j in range(i+1, n_vertices):
                vertice_2 = vertice_names[j]
                # print('-> j = {0}, v_2 = {1}'.format(j, vertice_2))

                # connecting lines
                if i+1 == j and vertice_1[0].name == vertice_2[0].name:
                    # print('\t\tLINE')
                    M[i,j] = - self.conductance_line
                    M[j,i] = - self.conductance_line
                # connecting tunneling contacts
                elif vertice_1[0].name == vertice_2[1].name and vertice_1[1].name == vertice_2[0].name:
                    # print('\t\tTUNNEL')
                    intersection_now = vertice_intersections[i]
                    area = intersection_now.area
                    
                    conductance_tunneling_now = self.conductance_tunneling * area

                    M[i,j] = - conductance_tunneling_now
                    M[j,i] = - conductance_tunneling_now
        
        # assigning main diagonal elements

        for i in range(n_vertices):
            column_sum = np.sum(M[:,i])
            M[i,i] = - column_sum

        # determining witch vertices have a current coming in/out
        # we pick the left-/rightmost vertices connected to the electrodes 

        index_left = None
        px_left = self.length

        index_right = None
        px_right = .0

        for v, intersec, i in zip(vertice_names, vertice_intersections, range(n_vertices)):
            line = v[0]

            x_center = np.mean(intersec.exterior.coords, axis=0)[0]
            
            if line.connects_left:
                if index_left is None or x_center < px_left:
                    index_left = i
                    px_left = x_center

            if line.connects_right:
                if index_right is None or x_center > px_right:
                    index_right = i
                    px_right = x_center


        current_vector = np.zeros((n_vertices,))
        current_vector[index_left] = CURRENT # in to the left
        current_vector[index_right] = -CURRENT # out to the right

        # earthing
        # we set the potential to zero at the vertice connected with the right electrode

        mode = 'delete'

        # print(index_left, ' ', index_right)

        possible_earthings = list(range(n_vertices))

        possible_earthings.remove(index_left)
        possible_earthings.remove(index_right)

        earthing_index = random.choice(possible_earthings)

        # define use indizes to decide what values to grab later
        # we need these to later take into account that the indizes change under certain earthings
        if mode == 'delete':
            if earthing_index < index_left:
                index_left_use = index_left - 1
            else:
                index_left_use = index_left
            
            if earthing_index < index_right:
                index_right_use = index_right - 1
            else:
                index_right_use = index_right

            # delete row
            M = np.delete(M, earthing_index, axis=0)
            # delete column
            M = np.delete(M, earthing_index, axis=1)
            # delete row in current vector
            current_vector = np.delete(current_vector, earthing_index, axis=0)
        elif mode == 'set':
            for i in range(n_vertices):
                M[i, earthing_index] = M[earthing_index, i] = .0
            M[earthing_index, earthing_index] = 1.
        else:
            print('ERROR: mode {0} not implemented'.format(mode))
            raise NotImplementedError


        # checking numerics
        if verbose:
            condition = np.linalg.cond(M)
            print('\tmatrix condition: {0:.2e}'.format(condition), end=' ')
            if condition < 10**15:
                print('(good)')
            else:
                print('(bad)')


        # solving for conductance

        M_inv = np.linalg.inv(M)

        potentials = M_inv @ current_vector


        if earthing_index == index_right and mode == 'delete':
            voltage = potentials[index_left_use]
        elif earthing_index == index_left and mode == 'delete':
            voltage = - potentials[index_right_use]
        else:
            voltage = potentials[index_left_use] - potentials[index_right_use]

        if verbose:
            print('\tvoltage: {0} V'.format(voltage))

        # calculate conductance
        # U = I * R <=> U * G = I <=> G = I / U

        G = CURRENT / voltage

        if verbose:
            print('\tconductance: {0} G0'.format(G))

        return G

    ###########################################
    # Visualization
    ###########################################
    
    def plot(self, filename, mode, border='tight'):
        fig, ax = plt.subplots()

        colors = ['C{0}'.format(i) for i in range(10)]


        if mode == 'normal':
            y_list = [l.y for l in self.lines]

            y_min = np.min(y_list)
            y_max = np.max(y_list)
            y_delta = y_max - y_min

            colormap = plt.get_cmap('hot')

            for l in self.lines:
                colorindex = (l.y - y_min) / y_delta * .7
                color = colormap(colorindex)

                p = mpatches.Polygon(
                    xy=l.polygon.exterior.coords,
                    facecolor=color,
                    zorder=l.y+1
                    )
                ax.add_patch(p)

        elif mode == 'clusters':
            # plotting lines
            for c, color in zip(self.clusters, colors):
                for l in c:
                    p = mpatches.Polygon(
                        xy=l.polygon.exterior.coords,
                        facecolor=color
                        )
                    ax.add_patch(p)
        else:
            print('ERROR: mode {0} not implemented'.format(mode))
            raise NotImplementedError

        # plotting intersections

        intersections = np.transpose(self.find_intersections())

        ax.plot(*intersections, linestyle=' ', marker='o', markeredgecolor='black', color='white',zorder=100)

        # background rectangle

        area_rectangle = mpatches.Rectangle(
            (0,0), self.length, self.width,
            color='lightgray',
            zorder=0
            )

        ax.add_patch(area_rectangle)


        # electrodes

        y_list = [.0, self.width]
        for x in [.0, self.length]:
            x_list = [x, x]
            ax.plot(
                x_list, 
                y_list, 
                color='gray', 
                zorder=1,
                linewidth=4.,
                linestyle='-'
                )
        
        # borders and axes labels

        if border == 'tight':
            border = np.max([self.width, self.length]) * .02
            ax.set_xlim(-border, self.length + border)
            ax.set_ylim(-border, self.width + border)
        else:
            pass

        ax.set_aspect('equal')
        ax.tick_params(
            direction='in',
            which='both'
            )
        
        ax.set_xlabel(r'$x$ [\AA]')
        ax.set_ylabel(r'$y$ [\AA]')

        # saving

        fig.savefig(filename, dpi=300, bbox_inches='tight')
        plt.close()
